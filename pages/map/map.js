//index.js
Page({
  data:{
    latitude: 31.968789,
    longitude: 118.798537,
    markers: [{
      iconPath: "./../../image/location.png",
      id: 0,
      latitude: 31.968789,
      longitude: 118.798537,
      width: 25,
      height: 40,
      label: {
        content: "我在这里",
        color: "#F5F5F5",
        fontSize: 15,
        bgColor: "#696969"
      }
    }],
    circles: [{
      longitude: 118.798537,
      latitude: 31.968789,
      fillColor: "#E6E6FAAA",
      color: "#000000DD",
      radius:100
    }]
  },
  wx_getLocation() {
    var myThis = this;
    wx.getLocation({
      type: 'gcj02',
      altitude: true,
      success:function(res) {
        myThis.setData({
          longitude: res.longitude,
          latitude: res.latitude,
          markers: [{
            iconPath: "./../../image/location.png",
            id: 0,
            latitude: res.latitude,
            longitude: res.longitude,
            width: 25,
            height: 40,
            label: {
              content: "我在这里",
              color: "#F5F5F5",
              fontSize: 15,
              bgColor: "#696969"
            }
          }],
          circles: [{
            longitude: res.longitude,
            latitude: res.latitude,
            fillColor: "#E6E6FAAA",
            color: "#000000DD",
            radius:200
          }]
        })
      },
      fail:function(err){
      }
    })
  } 
})